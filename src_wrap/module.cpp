/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/

#include <pybind11/pybind11.h>

#include "voxgrid/densefixedgrid.hpp"

namespace py = pybind11;

namespace voxgrid {
  void export_dense_array(py::module& m);
  void export_box(py::module &m);
  void export_fixed_grid(py::module &m);
  void export_densefixedgrid(py::module &m);
  //void export_dense_util(py::module& m);
  //void export_fixed_occmap(py::module& m);
  //void export_raycasting(py::module& m);
}

PYBIND11_MODULE(libvoxgrid, m) {
  m.doc() = "libvoxgrid";

  //openvdb::initialize();

  voxgrid::export_dense_array(m);
  voxgrid::export_box(m);
  voxgrid::export_fixed_grid(m);
  voxgrid::export_densefixedgrid(m);
  //voxgrid::export_dense_util(m);
  //voxgrid::export_fixed_occmap(m);
  //voxgrid::export_raycasting(m);
}
