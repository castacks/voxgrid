/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/


#include <string>

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>

#include <voxgrid/fixedgrid.hpp>
#include <voxgrid/densefixedgrid.hpp>


namespace voxgrid {

namespace py = pybind11;

template<class GridT>
void wrap(py::module& m, const char* name) {
  py::class_<GridT>(m, name)
      .def(py::init())
      .def(py::init<typename GridT::Vec,
                    typename GridT::VecIx,
                    typename GridT::Scalar>(),
           py::arg("center"),
           py::arg("dimension"),
           py::arg("resolution"))
      .def("dim", &GridT::dim, py::arg("i"))
      .def("grid_to_world", &GridT::grid_to_world, py::arg("grid_ix"))
      .def("is_inside_box", &GridT::is_inside_box, py::arg("pt"))
      .def("is_inside_grid", &GridT::is_inside_grid, py::arg("grid_ix"))
      .def("reset", &GridT::reset,
           py::arg("center"),
           py::arg("dimension"),
           py::arg("resolution"))
      .def("world_to_grid",
           &GridT::world_to_grid,
           py::arg("pt"))
      .def("multi_grid_to_world",
           &GridT::multi_grid_to_world,
           py::arg("grid_indices"))
      .def("multi_is_inside_box",
           &GridT::multi_is_inside_box,
           py::arg("points"))
      .def("get", &GridT::get, py::arg("grid_ix"))
      .def("set", &GridT::set, py::arg("grid_ix"), py::arg("value"))
      .def_property_readonly("storage", &GridT::storage)
      .def_property_readonly("center", &GridT::center)
      .def_property_readonly("dimension", &GridT::dimension)
      .def_property_readonly("max_pt", &GridT::max_pt)
      .def_property_readonly("min_pt", &GridT::min_pt)
      .def_property_readonly("num_cells", &GridT::num_cells)
      .def_property_readonly("origin", &GridT::origin)
      .def_property_readonly("radius", &GridT::radius)
      .def_property_readonly("resolution",&GridT::resolution)
      .def_property_readonly("box", &GridT::box)
      ;
}

void export_densefixedgrid(py::module& m) {

  wrap<DenseFixedGridMap2f>(m, "DenseFixedGridMap2f");
  wrap<DenseFixedGridMap3f>(m, "DenseFixedGridMap3f");

}

}
