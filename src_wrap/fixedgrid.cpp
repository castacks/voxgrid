/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/


#include <string>

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>

#include <voxgrid/fixedgrid.hpp>

#include "tinyformat.h"

namespace voxgrid {

namespace py = pybind11;

std::string FixedGrid2_repr(const FixedGrid2& self) {
  FixedGrid2::Vec c = self.center();
  Vec2Ix d = self.dimension();
  std::string repr = tfm::format("FixedGrid2(center=(%.3f, %.3f), dimension=(%d, %d), resolution=%f)",
                                 c.x(), c.y(), d.x(), d.y(), self.resolution());
  return repr;
}

std::string FixedGrid3_repr(const FixedGrid3& self) {
  FixedGrid3::Vec c = self.center();
  Vec3Ix d = self.dimension();
  std::string repr = tfm::format("FixedGrid3(center=(%.3f, %.3f, %.3f), dimension=(%d, %d %d), resolution=%f)",
                                 c.x(), c.y(), c.z(),
                                 d.x(), d.y(), d.z(),
                                 self.resolution());
  return repr;
}


template<class GridT>
void wrap(py::module& m, const char* name) {
  py::class_<GridT>(m, name)
      .def(py::init())
      .def(py::init<typename GridT::Vec,
                    typename GridT::VecIx,
                    typename GridT::Scalar>(),
           py::arg("center"),
           py::arg("dimension"),
           py::arg("resolution"))
      .def("dim", &GridT::dim, py::arg("i"))
      .def("grid_to_world", &GridT::grid_to_world, py::arg("grid_ix"))
      .def("is_inside_box", &GridT::is_inside_box, py::arg("pt"))
      .def("is_inside_grid", &GridT::is_inside_grid, py::arg("grid_ix"))
      .def("reset", &GridT::reset,
           py::arg("center"),
           py::arg("dimension"),
           py::arg("resolution"))
      .def("world_to_grid",
           &GridT::world_to_grid,
           py::arg("pt"))
      .def("multi_grid_to_world",
           &GridT::multi_grid_to_world,
           py::arg("grid_indices"))
      .def("multi_is_inside_box",
           &GridT::multi_is_inside_box,
           py::arg("points"))
      .def_property_readonly("center", &GridT::center)
      .def_property_readonly("dimension", &GridT::dimension)
      .def_property_readonly("max_pt", &GridT::max_pt)
      .def_property_readonly("min_pt", &GridT::min_pt)
      .def_property_readonly("num_cells", &GridT::num_cells)
      .def_property_readonly("origin", &GridT::origin)
      .def_property_readonly("radius", &GridT::radius)
      .def_property_readonly("resolution",&GridT::resolution)
      .def_property_readonly("box", &GridT::box)
      ;
}

void export_fixed_grid(py::module& m) {

  wrap<FixedGrid2>(m, "FixedGrid2");
  wrap<FixedGrid3>(m, "FixedGrid3");

#if 0
      .def("hash_to_grid", &FixedGrid3::hash_to_grid)
      .def("multi_hash_to_grid", &FixedGrid3::multi_hash_to_grid)
      .def("grid_to_hash", &FixedGrid3::grid_to_hash)
      .def("hash_to_local_grid", &FixedGrid3::hash_to_local_grid)
      .def("local_grid_to_hash", &FixedGrid3::local_grid_to_hash)
      .def("multi_grid_to_mem", &FixedGrid3::multi_grid_to_mem)
      .def("multi_grid_to_world", &FixedGrid3::multi_grid_to_world)
      .def("multi_is_inside_box", &FixedGrid3::multi_is_inside_box)
    ;
#endif
}

}
