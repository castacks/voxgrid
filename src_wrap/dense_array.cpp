#include <string>
#include <sstream>

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/numpy.h>

#include <boost/format.hpp>

#include <voxgrid/grid_types.hpp>
#include <voxgrid/dense_array.hpp>


namespace voxgrid {

namespace py = pybind11;

template <class ArrayT>
ArrayT* dense_array_from_ndarray(py::array_t<typename ArrayT::CellType, ArrayT::Dim> ndarr) {

  using VecIx = typename ArrayT::VecIx;
  using CellT = typename ArrayT::CellType;

  // TODO should we return holder
  VecIx shape;
  for (int i=0; i < ndarr.ndim(); ++i) {
    shape[i] = static_cast<gix_t>(ndarr.shape(i));
  }
  ArrayT* arr = new ArrayT(shape);

  // array is view
  //arr->reset(shape, static_cast<CellT *>(ndarr.mutable_data()));

  // copy, play it safe
  // TODO consider view with different return value policy
  std::copy(ndarr.data(),
            ndarr.data()+ndarr.size(),
            arr->begin());
  return arr;
}


template <class ArrayT>
py::buffer_info densearray_to_buffer_info(const ArrayT& arr) {
  using VecIx = typename ArrayT::VecIx;
  using CellT = typename ArrayT::CellType;

  py::buffer_info info;
  info.ptr = arr.data();
  info.itemsize = sizeof(CellT);
  info.format = py::format_descriptor<CellT>::format();

  py::print("format: ", info.format);

  info.ndim = ArrayT::Dim;
  // config strides
  for (int i=0; i < ArrayT::Dim; ++i) {
    info.shape.push_back(static_cast<size_t>(arr.dim(i)));
    info.strides.push_back(static_cast<size_t>(arr.stride(i)*sizeof(CellT)));
  }
  return info;
}


template <class ArrayT>
py::array_t<typename ArrayT::CellType> ndarray_view(py::object &self) {
  using VecIx = typename ArrayT::VecIx;
  using CellT = typename ArrayT::CellType;

  ArrayT& arr = self.cast<ArrayT&>();
  VecIx dims(arr.dimension());

  std::vector<size_t> shape, strides;
  for (int i=0; i < ArrayT::Dim; ++i) {
    shape.push_back(static_cast<size_t>(arr.dim(i)));
    strides.push_back(static_cast<size_t>(arr.stride(i)*sizeof(CellT)));
  }

  // last param tells numpy to tie lifetime of array to self
  py::array_t<CellT> view(shape, strides, arr.data(), self);
  return view;
}


template <class ArrayT>
void copy_to_ndarray(py::object &self,
                     py::array_t<typename ArrayT::CellType, py::array::c_style>& out) {

  ArrayT& arr = self.cast<ArrayT&>();

  if (out.ndim() != ArrayT::Dim) {
    throw py::value_error("bad number of dimensions");
  }

  for (int i=0; i < ArrayT::Dim; ++i) {
    if (arr.dim(i) != out.shape(i)) {
      throw py::value_error("dimensions don't match");
    }
  }

  std::copy(arr.cbegin(), arr.cend(), out.mutable_data());
}


template <class ArrayT>
void copy_from_ndarray(py::object &self,
                       py::array_t<typename ArrayT::CellType, py::array::c_style> arr) {

  ArrayT& self_arr = self.cast<ArrayT&>();

  if (arr.ndim() != ArrayT::Dim) {
    throw py::value_error("bad number of dimensions");
  }

  for (int i=0; i < ArrayT::Dim; ++i) {
    if (self_arr.dim(i) != arr.shape(i)) {
      throw py::value_error("dimensions don't match");
    }
  }

  std::copy(arr.data(), arr.data()+arr.size(), self_arr.begin());
}


template <class ArrayT>
py::class_<ArrayT> dense_array_wrap(py::module& m, const char* name) {
  py::class_<ArrayT> arr_class(m, name, py::buffer_protocol());
  arr_class.def(py::init<typename ArrayT::VecIx>())
           .def_static("from_ndarray", &dense_array_from_ndarray<ArrayT>)
           .def_buffer(&densearray_to_buffer_info<ArrayT>)
           .def_property_readonly("dimension", &ArrayT::dimension)
           .def("ndarray_view", &ndarray_view<ArrayT>)
           .def("copy_to_ndarray", &copy_to_ndarray<ArrayT>)
           .def("copy_from_ndarray", &copy_from_ndarray<ArrayT>)
           ;
  return arr_class;
}

template <class T>
py::array_t<T, 3> dense_array2vec2X_to_tensor(const DenseArray<vec2_t<T>, 2>& arr) {
  // TODO view version
  // TODO dim ordering
  using VecIx = typename DenseArray<vec2_t<T>, 2>::VecIx;

  py::array_t<T, 3> out({2, arr.dim(0), arr.dim(1)});
  auto out_buf = out.mutable_unchecked();
  for (int i=0; i < arr.dim(0); ++i) {
    for (int j=0; j < arr.dim(1); ++j) {
      const vec2_t<T>& vec(arr.get(VecIx(i, j)));
      //py::print("vec.x", vec.x, "vec.y", vec.y);
      out_buf(0, i, j) = vec.x;
      out_buf(1, i, j) = vec.y;
    }
  }
  return out;
}



void export_dense_array(py::module &m) {
  dense_array_wrap<DenseArray2f>(m, "DenseArray2f");
  dense_array_wrap<DenseArray3f>(m, "DenseArray3f");

  dense_array_wrap<DenseArray2i>(m, "DenseArray2i");
  dense_array_wrap<DenseArray3i>(m, "DenseArray3i");

  PYBIND11_NUMPY_DTYPE(vec2i_t, x, y);
  auto da2Veci = dense_array_wrap<DenseArray2Vec2i>(m, "DenseArray2Vec2i");
  da2Veci.def("to_tensor", &dense_array2vec2X_to_tensor<int>);
  dense_array_wrap<DenseArray3Vec2i>(m, "DenseArray3Vec2i");

  PYBIND11_NUMPY_DTYPE(vec2f_t, x, y);
  auto da2Vecf = dense_array_wrap<DenseArray2Vec2f>(m, "DenseArray2Vec2f");
  da2Vecf.def("to_tensor", &dense_array2vec2X_to_tensor<float>);
  dense_array_wrap<DenseArray3Vec2f>(m, "DenseArray3Vec2f");

  PYBIND11_NUMPY_DTYPE(vec3f_t, x, y, z);
  dense_array_wrap<DenseArray2Vec3f>(m, "DenseArray2Vec3f");
  dense_array_wrap<DenseArray3Vec3f>(m, "DenseArray3Vec3f");

}

}
