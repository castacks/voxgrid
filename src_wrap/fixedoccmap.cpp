#include <string>

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>

#include <ros/ros.h>
#include <boost/format.hpp>

#include <scrollgrid/fixedgrid.hpp>

#include <scrollgrid/fixedoccmap.hpp>

namespace ca { namespace pyvox {

namespace py = pybind11;


void exportFixedOccMap(py::module& m) {

  using namespace ca::scrollgrid;

  py::class_<FixedBinMap2f>(m, "FixedBinMap2f")
      //.def(py::init<const csg::FixedGrid2f&>())
      .def(py::init<typename FixedBinMap2f::Vecf, typename FixedBinMap2f::VecIx, float>(), "center, dimension, resolution")
      .def("reset", &FixedBinMap2f::reset)
      .def("update", &FixedBinMap2f::update)
      .def("multi_update", &FixedBinMap2f::multi_update)
      .def("multi_update_single_vp", &FixedBinMap2f::multi_update_single_vp)
      .def("get_storage", &FixedBinMap2f::get_storage)
      .def("get_grid", &FixedBinMap2f::get_grid)
      .def("decay", &FixedBinMap2f::decay)
    ;

  py::class_<FixedHitPassMap2i>(m, "FixedHitPassMap2i")
      .def(py::init<typename FixedHitPassMap2i::Vecf, typename FixedHitPassMap2i::VecIx, float>(), "center, dimension, resolution")
      .def("reset", &FixedHitPassMap2i::reset)
      .def("update", &FixedHitPassMap2i::update)
      .def("multi_update", &FixedHitPassMap2i::multi_update)
      .def("multi_update_single_vp", &FixedHitPassMap2i::multi_update_single_vp)
      .def("get_storage", &FixedHitPassMap2i::get_storage)
      .def("get_grid", &FixedHitPassMap2i::get_grid)
      .def("decay", &FixedHitPassMap2i::decay)
    ;

  py::class_<FixedBinMap3f>(m, "FixedBinMap3f")
      .def(py::init<typename FixedBinMap3f::Vecf, typename FixedBinMap3f::VecIx, float>(), "center, dimension, resolution")
      .def("reset", &FixedBinMap3f::reset)
      .def("update", &FixedBinMap3f::update)
      .def("multi_update", &FixedBinMap3f::multi_update)
      .def("multi_update_single_vp", &FixedBinMap3f::multi_update_single_vp)
      .def("get_storage", &FixedBinMap3f::get_storage)
      .def("get_grid", &FixedBinMap3f::get_grid)
      .def("decay", &FixedBinMap3f::decay)
    ;

  py::class_<FixedHitPassMap3i>(m, "FixedHitPassMap3i")
      .def(py::init<typename FixedHitPassMap3i::Vecf, typename FixedHitPassMap3i::VecIx, float>(), "center, dimension, resolution")
      .def("reset", &FixedHitPassMap3i::reset)
      .def("update", &FixedHitPassMap3i::update)
      .def("multi_update", &FixedHitPassMap3i::multi_update)
      .def("multi_update_single_vp", &FixedHitPassMap3i::multi_update_single_vp)
      .def("get_storage", &FixedHitPassMap3i::get_storage)
      .def("get_grid", &FixedHitPassMap3i::get_grid)
      .def("decay", &FixedHitPassMap3i::decay)
    ;

}

} }
