#include <string>

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>

#include <ros/ros.h>
#include <boost/format.hpp>

#include <scrollgrid/ray.hpp>
#include <scrollgrid/raycasting.hpp>

namespace ca { namespace pyvox {

namespace py = pybind11;
namespace csg = ca::scrollgrid;


void exportRaycasting(py::module& m) {

  py::class_<csg::Ray2f>(m, "Ray2f")
      .def(py::init<Eigen::Vector2f, Eigen::Vector2f>())
      .def("point_at", &csg::Ray2f::point_at)
      .def_readonly("tmin", &csg::Ray2f::tmin)
      .def_readonly("tmax", &csg::Ray2f::tmax)
      ;

}

} } /* ca  */
