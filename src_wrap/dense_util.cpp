/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>

#include <string>
#include <unordered_map>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "pyvox/pyassert.h"

namespace ca { namespace pyvox {

namespace py = pybind11;

inline
uint64_t grid_to_hash(const Eigen::Vector3i& ijk) {
  // assuming ijk are non-negative and in a reasonable range
  uint64_t hi = static_cast<uint64_t>(ijk[0]);
  uint64_t hj = static_cast<uint64_t>(ijk[1]);
  uint64_t hk = static_cast<uint64_t>(ijk[2]);
  uint64_t h = (hi << 48) | (hj << 32) | (hk << 16);
  return h;
}

inline
Eigen::Vector3i hash_to_grid(uint64_t hix) {
  uint64_t hi = (hix & 0xffff000000000000) >> 48;
  uint64_t hj = (hix & 0x0000ffff00000000) >> 32;
  uint64_t hk = (hix & 0x00000000ffff0000) >> 16;
  Eigen::Vector3i grid_ix(hi, hj, hk);
  return grid_ix;
}

Eigen::MatrixXf arr_to_one_hot_aux( Eigen::VectorXi arr, int num_labels) {
  Eigen::MatrixXf out( num_labels, arr.size() );
  out.setZero();
  for ( int j=0; j<arr.size(); ++j) {
    int i = arr(j);
    out(i, j) = 1.0*(i != 0);
  }
  return out;
}

py::tuple pool_labels_sparse(Eigen::Matrix3Xi ijk_sparse,
                             Eigen::VectorXi labels_sparse,
                             int num_labels,
                             Eigen::Vector3i spatial_dims) {

  typedef std::unordered_map<uint64_t, std::vector<int> > MapType;

  py::tuple out;

  MapType label_map;
  for (int col=0; col < ijk_sparse.cols(); ++col) {
    Eigen::Vector3i gix = ijk_sparse.col( col );
    if ( gix[0] >= spatial_dims[0] ||
         gix[1] >= spatial_dims[1] ||
         gix[2] >= spatial_dims[2] ) {
      continue;
    }
    uint64_t hix = grid_to_hash( gix );
    std::vector<int>& cell_labels(label_map[hix]);
    if (cell_labels.empty()) {
      cell_labels.resize( num_labels );
    }
    // labels start from 1
    int lbl = labels_sparse[col]-1;
    cell_labels[lbl] += 1;
  }
  Eigen::MatrixXi vals(num_labels, label_map.size());
  Eigen::Matrix3Xi ijk2(3, label_map.size());
  vals.setZero();
  ijk2.setZero();
  int col = 0;
  for (MapType::iterator itr = label_map.begin();
       itr != label_map.end();
       ++itr,
       ++col) {
    const std::vector<int>& cell_labels(itr->second);
    for (int row=0; row < num_labels; ++row) {
      vals( row, col ) = cell_labels[row];
      ijk2.col( col ) = hash_to_grid( itr->first );
    }
  }

  out = py::make_tuple(ijk2, vals);
  return out;
}

void exportDenseUtil(py::module& m) {
  m.def("arr_to_one_hot_aux", &arr_to_one_hot_aux);
  m.def("pool_labels_sparse", &pool_labels_sparse);
}

} }
