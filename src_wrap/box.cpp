/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>

#include <voxgrid/box.hpp>

namespace voxgrid {

namespace py = pybind11;

void export_box(py::module& m) {

  py::class_<Box3f>(m, "Box3f")
      .def(py::init<Box3f::Vec, Box3f::Vec>())
      .def("set_center", &Box3f::set_center)
      .def("center", &Box3f::center)
      .def("translate", &Box3f::translate)
      .def("contains", &Box3f::contains)
      .def("min_pt", &Box3f::min_pt)
      .def("max_pt", &Box3f::max_pt)
      .def("bound", &Box3f::bound)
      .def("set_bound", &Box3f::set_bound)
      .def("set_min_pt", &Box3f::set_min_pt)
      .def("set_max_pt", &Box3f::set_max_pt)
      ;

  py::class_<Box2f>(m, "Box2f")
      .def(py::init<Box2f::Vec, Box2f::Vec>())
      .def("set_center", &Box2f::set_center)
      .def("center", &Box2f::center)
      .def("translate", &Box2f::translate)
      .def("contains", &Box2f::contains)
      .def("min_pt", &Box2f::min_pt)
      .def("max_pt", &Box2f::max_pt)
      .def("bound", &Box2f::bound)
      .def("set_bound", &Box2f::set_bound)
      .def("set_min_pt", &Box2f::set_min_pt)
      .def("set_max_pt", &Box2f::set_max_pt)
      ;
}

}
