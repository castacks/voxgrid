#define PICOBENCH_IMPLEMENT_WITH_MAIN
#include "picobench.hpp"

#include <voxgrid/dense_array.hpp>

#include <cstdlib>
#include <iostream>

void withvec(picobench::state& s) {
  using namespace voxgrid;

  DenseArray3f da(Vec3Ix(10, 20, 30));
  for (int i=0; i < da.num_cells(); ++i) {
    da[i] = float(std::rand())/RAND_MAX;
  }

  float sum = 0;
  for (auto _ : s) {
    for (int i=0; i < 8; ++i) {
      for (int j=10; j < 12; ++j) {
        for (int k=2; k < 28; ++k) {
          float x = da.get(Vec3Ix(i, j, k));
          sum += x;
        }
      }
    }
  }
  std::cerr << "sum = " << sum << std::endl;

}
PICOBENCH(withvec).samples(10);

void withvariadic(picobench::state& s) {
  using namespace voxgrid;

  DenseArray3f da(Vec3Ix(10, 20, 30));
  for (int i=0; i < da.num_cells(); ++i) {
    da[i] = float(std::rand())/RAND_MAX;
  }
  float sum = 0;
  for (auto _ : s) {
    for (int i=0; i < 8; ++i) {
      for (int j=10; j < 12; ++j) {
        for (int k=2; k < 28; ++k) {
          float x = da(i, j, k);
          sum += x;
        }
      }
    }
  }
  std::cerr << "sum = " << sum << std::endl;

}
PICOBENCH(withvariadic).samples(10);


// results: with build type, Release, it's a wash.
// with default build type, variadic is 3x faster.
// presumably eigen is doing some bound checking.
