#include <voxgrid/dense_array.hpp>
#include <voxgrid/box.hpp>

#include <Eigen/Core>
#include <iostream>

int main(int argc, char *argv[]) {

  using namespace voxgrid;

  DenseArray3f da(Vec3Ix(10, 20, 30));
  float& x = da.get(Vec3Ix(4, 8, 12));
  std::cerr << "x = " << x << std::endl;
  x = 20;
  float& y = da.get(Vec3Ix(4, 8, 12));
  std::cerr << "y = " << x << std::endl;

  float z = da(4, 8, 12);
  std::cerr << "z = " << z << std::endl;

  da(4, 8, 12) = 3.;

  using Vec2 = Box2f::Vec;
  using Vec3 = Box3f::Vec;
  Box2f box2(Vec2(-1., -1), Vec2(1., 2.));
  Box3f box3(Vec3(-1., -1, -1.), Vec3(1., 2., 3.));


  Vec2 p1, p2;
  box2.clip_line(Vec2(0., 0.), Vec2(1., 3.), p1, p2);

  std::cerr << "p1 = " << p1 << std::endl;
  std::cerr << "p2 = " << p2 << std::endl;

  Vec3 q1, q2;
  box3.clip_line(Vec3(0., 0., 0.), Vec3(1., 3., 4.), q1, q2);
  std::cerr << "q1 = " << q1 << std::endl;
  std::cerr << "q2 = " << q2 << std::endl;

  return 0;
}
