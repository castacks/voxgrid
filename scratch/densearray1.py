
import gc
import numpy as np
import voxgrid

#da = voxgrid.DenseArray2f((10, 10))
#print da

#da2 = voxgrid.DenseArray2f(np.array((10., 10.), dtype='f4'))
da2 = voxgrid.DenseArray2f.from_ndarray(np.zeros((2, 2), dtype='f4'))

del da2
#gc.collect()

da2vec2 = voxgrid.DenseArray2Vec2f((2, 2))
print da2vec2

ndvec2 = da2vec2.ndarray_view()
print ndvec2.dtype
ndvec2['x'][0, 0] = 1.
ndvec2['y'][1, 1] = 2.

print ndvec2

print da2vec2.dimension

ndtensor = da2vec2.to_tensor()
print ndtensor

da3vec2 = voxgrid.DenseArray3Vec2i((10, 20, 20))
print da3vec2.dimension

print da3vec2.ndarray_view()

