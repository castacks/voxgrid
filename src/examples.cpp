/**
 * Copyright (c) 2015 Carnegie Mellon University, Daniel Maturana <dimatura@cmu.edu>
 *
 * For License information please see the LICENSE file in the root directory.
 *
 */

#include "voxgrid/dense_array.hpp"
#include "voxgrid/raycasting.hpp"
#include "voxgrid/fixedgrid.hpp"
#include "voxgrid/fixedoccmap.hpp"

int main(int argc, char *argv[]) {

  namespace sg = voxgrid;

  //ca::Vec2Ix dims(28, 28);
  //hpmap.init(dims);

  sg::OccMap<sg::HitPassI4, sg::FixedGrid, sg::DenseArray, sg::HitPassUpdater, 2> hpmap2;
  sg::OccMap<float, sg::FixedGrid, sg::DenseArray, sg::BinaryFloatUpdater, 2> occmap2;

  return 0;
}
