cmake_minimum_required(VERSION 2.8.3)
project(voxgrid)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

find_package(catkin REQUIRED COMPONENTS
    cmake_modules
    eigen_catkin
    pybind11
    pybind11_catkin
    roscpp
    rospy
)

find_package(NUMPY REQUIRED)
#find_package(pybind11 REQUIRED)
#find_package(Eigen3 3.3.1 REQUIRED)

catkin_python_setup()

# TODO: fill in what other packages will need to use this package
catkin_package(
    INCLUDE_DIRS include
    LIBRARIES voxgrid
    CATKIN_DEPENDS rospy roscpp eigen_catkin
    DEPENDS NUMPY
)

pybind_add_module(libvoxgrid
    src_wrap/module.cpp
    src_wrap/dense_array.cpp
    src_wrap/box.cpp
    src_wrap/fixedgrid.cpp
    src_wrap/densefixedgrid.cpp
)

include_directories(include
                    ${catkin_INCLUDE_DIRS}
                    #${EIGEN3_INCLUDE_DIR}
                    )

target_link_libraries(libvoxgrid
    PRIVATE
    ${catkin_LIBRARIES}
)


add_executable(densearray1 scratch/densearray1.cpp)
target_link_libraries(densearray1 ${catkin_LIBRARIES})

add_executable(densearray1bench scratch/densearray1bench.cpp)
target_link_libraries(densearray1bench ${catkin_LIBRARIES})
