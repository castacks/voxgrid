/**
 * Copyright (c) 2015 Carnegie Mellon University, Daniel Maturana <dimatura@cmu.edu>
 *
 * For License information please see the LICENSE file in the root directory.
 *
 */

#ifndef GRID_TYPES_HPP_HJ46RUAT
#define GRID_TYPES_HPP_HJ46RUAT

#include <cstdint>

#include <Eigen/Core>

namespace voxgrid {

// a compact bitwise representation of ijk coordinates
typedef std::uint64_t hash_ix_t;
typedef Eigen::Matrix<hash_ix_t, 1, Eigen::Dynamic> HashIxVector;

// linear index type
using mix_t = std::int64_t;
template<int D>
using VecDMIx = Eigen::Matrix<mix_t, 1, D>;
using Vec2MIx = VecDMIx<2>;
using Vec3MIx = VecDMIx<3>;
using Vec4MIx = VecDMIx<4>;
using VecXMIx = Eigen::Matrix<mix_t, 1, Eigen::Dynamic>;

// indexes grids (i, j, k)
using gix_t = std::int32_t;
template<int D>
using VecDIx = Eigen::Matrix<gix_t, 1, D>;
using Vec2Ix = VecDIx<2>;
using Vec3Ix = VecDIx<3>;
using Vec4Ix = VecDIx<4>;

// matrix of indices
template<int D>
using MatDIx = Eigen::Matrix<gix_t, Eigen::Dynamic, D, Eigen::RowMajor>;
using Mat2Ix = MatDIx<2>;
using Mat3Ix = MatDIx<3>;
using Mat4Ix = MatDIx<4>;

// POD types for cell contents
template <class T>
struct vec2_t {
  typedef T type;
  T x;
  T y;
};

typedef vec2_t<std::int32_t> vec2i_t;
typedef vec2_t<float> vec2f_t;

// TODO should we pad?
struct vec3f_t {
  float x;
  float y;
  float z;
};

}

#endif /* end of include guard: GRID_TYPES_HPP_HJ46RUAT */
