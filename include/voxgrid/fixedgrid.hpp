/**
 * Copyright (c) 2015 Carnegie Mellon University, Daniel Maturana <dimatura@cmu.edu>
 *
 * For License information please see the LICENSE file in the root directory.
 *
 */

#ifndef FIXEDGRID_HPP_TL9WFUEA
#define FIXEDGRID_HPP_TL9WFUEA

#include <cstdint>

#include <vector>
#include <algorithm>
#include <memory>

#include <Eigen/Core>
#include <Eigen/Dense>

#include "voxgrid/grid_types.hpp"
#include "voxgrid/box.hpp"

namespace voxgrid {

template<class ScalarT, int DimT>
class FixedGrid {
public:
  static constexpr int Dim = DimT;

  using Scalar = ScalarT;
  using Vec = Eigen::Matrix<Scalar, 1, Dim>;
  using MatS = Eigen::Matrix<Scalar, Eigen::Dynamic, Dim, Eigen::RowMajor>;
  using VecIx = VecDIx<Dim>;
  using MatIx = MatDIx<Dim>;
  using Ptr = std::shared_ptr<FixedGrid>;
  using ConstPtr = std::shared_ptr<const FixedGrid>;
  using BoxType = Box<Scalar, Dim>;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  FixedGrid() :
      box_(),
      origin_(Vec::Zero()),
      min_world_corner_ix_(VecIx::Zero()),
      dimension_(VecIx::Zero()),
      num_cells_(0),
      resolution_(0.)
  {
  }

  /**
   * @param center: center of the grid in global frame
   * @param dimension: number of grid cells along each coordinate
   * @param resolution: size of each grid cell side. they are cubic.
   * Default assumes so-called "ZYX" layout, i.e. z changes fastest.
   */
  FixedGrid(const Vec& center,
            const VecIx& dimension,
            Scalar resolution) :
      box_(center-(dimension.template cast<Scalar>()*resolution)/2,
           center+(dimension.template cast<Scalar>()*resolution)/2),
      origin_(center-box_.radius()),
      dimension_(dimension),
      num_cells_(dimension.prod()),
      resolution_(resolution) {
    this->calc_min_corner_ix();
  }

  FixedGrid(const FixedGrid& other) :
      box_(other.box_),
      origin_(other.origin_),
      min_world_corner_ix_(other.min_world_corner_ix_),
      dimension_(other.dimension_),
      num_cells_(other.num_cells_),
      resolution_(other.resolution_) {
  }

  FixedGrid& operator=(const FixedGrid& other) {
    if (this==&other) { return *this; }
    box_ = other.box_;
    origin_ = other.origin_;
    min_world_corner_ix_ = other.min_world_corner_ix_;
    dimension_ = other.dimension_;
    num_cells_ = other.num_cells_;
    resolution_ = other.resolution_;
    return *this;
  }

  virtual ~FixedGrid() { }

public:
  /**
   * see ctor for params
   */
  void reset(const Vec& center,
             const VecIx& dimension,
             Scalar resolution) {
    box_.set_center(center);
    box_.set_radius((dimension.template cast<Scalar>()*resolution)/2);
    origin_ = center - box_.radius();
    dimension_ = dimension;
    num_cells_ = dimension.prod();
    resolution_ = resolution;
  }

  void copy_from(FixedGrid& other) {
    this->reset(other.center(),
                other.dimension(),
                other.resolution());
  }

  /**
   * Is pt inside 3D box containing grid?
   * @param pt point in same frame as center (probably world_view)
   */
  bool is_inside_box(const Vec& pt) const {
    return box_.contains(pt);
  }

  Eigen::VectorXi multi_is_inside_box(const MatS& pts) const {
    Eigen::VectorXi out(pts.rows());
    for (int i=0; i < pts.rows(); ++i) {
      const Vec& pt(pts.row(i));
      out[i] = static_cast<int>(box_.contains(pt));
    }
    return out;
  }

  /**
   * is i, j, (k) inside the grid limits?
   */
  bool is_inside_grid(const VecIx& grid_ix) const {
    return ((grid_ix.array() >= 0).all() &&
            (grid_ix.array() < dimension_.array()).all());
  }

  /**
   * Given position in world coordinates, return grid coordinates.
   * Note: does not check if point is inside grid.
   */
  VecIx world_to_grid(const Vec& x) const {
    Vec tmp = ((x - origin_).array() - 0.5*resolution_)/resolution_;
    //return tmp.cast<gix_t>();
    return VecIx(tmp.array().round().template cast<gix_t>());
  }

  Vec grid_to_world(const VecIx& grid_ix) const {
    Vec w((grid_ix.template cast<Scalar>()*resolution_ + origin_).array() + 0.5*resolution_);
    return w;
  }

  MatS multi_grid_to_world(const MatIx& grid_indices) const {
    //MatS out(dim_t::value, grid_indices.rows());
    MatS out(grid_indices.rows(), grid_indices.cols());
    for (int ix=0; ix < grid_indices.rows(); ++ix) {
      VecIx gix(grid_indices.row(ix));
      out.row(ix) = this->grid_to_world(gix);
    }
    return out;
  }

 public:
  gix_t dim(int i) const { return dimension_[i]; }
  gix_t first(int i) const { return 0; }
  gix_t last(int i) const { return dimension_[i]; }

  const VecIx& dimension() const { return dimension_; }
  const Vec& radius() const { return box_.radius(); }
  const Vec& origin() const { return origin_; }
  Vec min_pt() const { return box_.min_pt(); }
  Vec max_pt() const { return box_.max_pt(); }
  const Vec& center() const { return box_.center(); }
  Scalar resolution() const { return resolution_; }
  BoxType box() const { return box_; }

  /**
   * rather esoteric, related to hashing.
   * TODO move elsewhere
   */
  VecIx min_world_corner_ix() { return min_world_corner_ix_; }

  gix_t num_cells() const { return num_cells_; }

 private:
  void calc_min_corner_ix() {
    // set the "minimum possible" ijk, assuming the grid
    // won't stray "too far" from the initial position.
    // too far == more than 2^15 grid cells.
    // so if you voxel resolution is 1 cm, 327.68 m.
    Scalar m = -static_cast<Scalar>(std::numeric_limits<uint16_t>::max()/2)*resolution_;
    Vec mn; mn.fill(m);
    mn += box_.center();
    min_world_corner_ix_ = this->world_to_grid(mn);
  }

 private:
  // box enclosing grid. In whatever coordinates were given (probably
  // world_view)
  //Box<Scalar, Dim> box_;
  BoxType box_;

  // static origin of the grid coordinate system.
  Vec origin_;

  // minimum world corner in ijk. used for hash
  VecIx min_world_corner_ix_;

  // number of grid cells along each axis
  VecIx dimension_;

  // number of cells
  gix_t num_cells_;

  // size of grid cells
  Scalar resolution_;
};

// I consider float to be the default.
typedef FixedGrid<float, 2> FixedGrid2;
typedef FixedGrid<float, 3> FixedGrid3;

// these are here for consistency.
typedef FixedGrid<float, 2> FixedGrid2f;
typedef FixedGrid<float, 3> FixedGrid3f;

typedef FixedGrid<double, 2> FixedGrid2d;
typedef FixedGrid<double, 3> FixedGrid3d;

}

#endif
