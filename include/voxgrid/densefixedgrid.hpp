#ifndef DENSEFIXEDGRID_HPP_54BKPJIW
#define DENSEFIXEDGRID_HPP_54BKPJIW

#include <cstdint>

#include <vector>
#include <algorithm>
#include <memory>

#include <Eigen/Core>
#include <Eigen/Dense>

#include "voxgrid/grid_types.hpp"
#include "voxgrid/fixedgrid.hpp"
#include "voxgrid/dense_array.hpp"

namespace voxgrid {

template<class ScalarT,
         class CellT,
         int DimT,
         template <typename, int> class FixedGridT,
         template <typename, int> class StorageT>
class DenseFixedGridMap {
 public:
  static constexpr int Dim = DimT;
  using Scalar = ScalarT;
  using Vec = Eigen::Matrix<Scalar, 1, Dim>;
  using MatS = Eigen::Matrix<Scalar, Eigen::Dynamic, Dim, Eigen::RowMajor>;
  using VecIx = VecDIx<Dim>;
  using VecMIx = VecDMIx<Dim>;
  using MatIx = MatDIx<Dim>;

  using Ptr = std::shared_ptr<DenseFixedGridMap>;
  using ConstPtr = std::shared_ptr<const DenseFixedGridMap>;

  using CellType = CellT;
  using StorageType = StorageT<CellType, Dim>;
  using FixedGridType = FixedGridT<Scalar, Dim>;
  using BoxType = typename FixedGridType::BoxType;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  DenseFixedGridMap() :
      grid_(),
      storage_()
  {
  }

  DenseFixedGridMap(const Vec& center,
                    const VecIx& dimension,
                    Scalar resolution) :
      grid_(center, dimension, resolution),
      storage_(dimension)
  {

  }

  void reset(const Vec& center,
             const VecIx& dimension,
             Scalar resolution) {
      grid_.reset(center, dimension, resolution);
      storage_.reset(dimension);
  }

  /**
   * Is pt inside 3D box containing grid?
   * @param pt point in same frame as center (probably world_view)
   */
  bool is_inside_box(const Vec& pt) const {
    return grid_.is_inside_box(pt);
  }

  Eigen::VectorXi multi_is_inside_box(const MatS& pts) {
    return grid_.multi_is_inside_box(pts);
  }

  /**
   * is i, j, (k) inside the grid limits?
   */
  bool is_inside_grid(const VecIx& grid_ix) const {
    return grid_.is_inside_grid(grid_ix);
  }

  /**
   * Given position in world coordinates, return grid coordinates.
   * Note: does not check if point is inside grid.
   */
  VecIx world_to_grid(const Vec& x) const {
    return grid_.world_to_grid(x);
  }

  Vec grid_to_world(const VecIx& grid_ix) const {
    return grid_.grid_to_world(grid_ix);
  }

  MatS multi_grid_to_world(const MatIx& grid_indices) const {
    return grid_.multi_grid_to_world(grid_indices);
  }

  CellType& get(const VecIx& grid_ix) {
    // TODO how would this work for scrollgrids?
    return storage_.get(grid_ix);
  }

  void set(const VecIx& grid_ix, const CellType& val) {
    storage_.set(grid_ix, val);
  }

  // TODO rethink interface
  StorageType& storage() {
    return storage_;
  }

 public:
  gix_t dim(int i) const { return grid_.dim(i); }
  gix_t first(int i) const { return grid_.first(i); }
  gix_t last(int i) const { return grid_.lastt(i); }

  const VecIx& dimension() const { return grid_.dimension(); }
  const Vec& radius() const { return grid_.radius(); }
  const Vec& origin() const { return grid_.origin(); }
  Vec min_pt() const { return grid_.min_pt(); }
  Vec max_pt() const { return grid_.max_pt(); }
  const Vec& center() const { return grid_.center(); }
  Scalar resolution() const { return grid_.resolution(); }
  BoxType box() const { return grid_.box(); }
  VecIx min_world_corner_ix() { return grid_.min_world_corner_ix(); }
  gix_t num_cells() const { return grid_.num_cells(); }


private:
  StorageType storage_;
  FixedGridType grid_;

};

// I consider float to be the default.

typedef DenseFixedGridMap<float, float, 2, FixedGrid, DenseArray> DenseFixedGridMap2f;
typedef DenseFixedGridMap<float, float, 3, FixedGrid, DenseArray> DenseFixedGridMap3f;

typedef DenseFixedGridMap<float, std::int32_t, 2, FixedGrid, DenseArray> DenseFixedGridMap2i;
typedef DenseFixedGridMap<float, std::int32_t, 3, FixedGrid, DenseArray> DenseFixedGridMap3i;

typedef DenseFixedGridMap<float, vec2i_t, 2, FixedGrid, DenseArray> DenseFixedGridMap2Vec2i;
typedef DenseFixedGridMap<float, vec2i_t, 3, FixedGrid, DenseArray> DenseFixedGridMap3Vec2i;

typedef DenseFixedGridMap<float, vec2f_t, 2, FixedGrid, DenseArray> DenseFixedGridMap2Vec2f;
typedef DenseFixedGridMap<float, vec2f_t, 3, FixedGrid, DenseArray> DenseFixedGridMap3Vec2f;

typedef DenseFixedGridMap<float, vec3f_t, 2, FixedGrid, DenseArray> DenseFixedGridMap2Vec3f;
typedef DenseFixedGridMap<float, vec3f_t, 3, FixedGrid, DenseArray> DenseFixedGridMap3Vec3f;


}

#endif /* end of include guard: DENSEFIXEDGRID_HPP_54BKPJIW */
