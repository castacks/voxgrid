#ifndef FIXEDOCCMAP_HPP_I9FPFYWV
#define FIXEDOCCMAP_HPP_I9FPFYWV

#include <memory>
#include <limits>

#include <voxgrid/fixedgrid.hpp>
#include <voxgrid/dense_array.hpp>
#include <voxgrid/raycasting.hpp>

/**
 * FixedGrid + DenseArray = static occmap.
 */
namespace voxgrid {

template<class Scalar>
constexpr Scalar logit(Scalar x) {
  // beware edge cases
  return std::log(x/(1.0 - x));
}

class HitPassUpdater {
public:

  void init(HitPassI4& cell) {
    cell.hits = 0;
    cell.passes = 0;
  }

  void update_pos(HitPassI4& cell) {
    cell.hits = std::min(cell.hits+1, std::numeric_limits<int32_t>::max()-1);
  }

  void update_neg(HitPassI4& cell) {
    cell.passes = std::min(cell.passes+1, std::numeric_limits<int32_t>::max()-1);
  }

  void update_decay(HitPassI4& cell) {
    cell.hits = std::max(cell.hits-1, 0);
    cell.passes = std::max(cell.passes-1, 0);
  }

};


class BinaryFloatUpdater {
public:
  // constants from octomap paper
  // TODO: static constexpr is giving me linkage errors.
  // seems like it's ok in c++17, uncertain otherwise.

#if 0
  static constexpr float UNKNOWN_PROB = 0.5;
  static constexpr float OCCUPIED_PROB = 0.97;
  static constexpr float FREE_PROB = 0.12;

  static constexpr float UPDATE_POS_PROB = 0.7;
  static constexpr float UPDATE_NEG_PROB = 0.4;

  static constexpr float UNKNOWN = logit(UNKNOWN_PROB);
  static constexpr float OCCUPIED = logit(OCCUPIED_PROB);
  static constexpr float FREE = logit(FREE_PROB);

  static constexpr float UPDATE_POS = logit(UPDATE_POS_PROB);
  static constexpr float UPDATE_NEG = logit(UPDATE_NEG_PROB);

  // NOTE: this is multiplicative and should be 0 < x < 1
  static constexpr float UPDATE_DECAY = 0.95f;
#endif

#if 0
  static constexpr float UNKNOWN = 0.0; // 0.5
  static constexpr float OCCUPIED = 3.5; // 0.97
  static constexpr float FREE = -2.; // 0.12
  static constexpr float UPDATE_POS = 0.85; // 0.7
  static constexpr float UPDATE_NEG = -0.4; // 0.4
  static constexpr float UPDATE_DECAY = 0.95; // 0.4
#endif

  float UNKNOWN = 0.0; // 0.5
  float OCCUPIED = 3.5; // 0.97
  float FREE = -2.; // 0.12
  float UPDATE_POS = 0.85; // 0.7
  float UPDATE_NEG = -0.4; // 0.4
  float UPDATE_DECAY = 0.95; // 0.4


public:

  void init(float& cell) {
    cell = UNKNOWN;
  }

  void update_pos(float& cell) {
    cell = std::min(cell + UPDATE_POS, OCCUPIED);
  }

  void update_neg(float& cell) {
    cell = std::max(cell + UPDATE_NEG, FREE);
  }

  void update_decay(float& cell) {
    // push towards unknown (default 0 in the log domain)
    // I have no theoretical justification. there's an exercise
    // at the end of thurn robotics book.
    cell = cell*UPDATE_DECAY;
  }
};


class BinaryUint8Updater {
public:
  static constexpr uint8_t UNKNOWN = 127;
  static constexpr uint8_t OCCUPIED = 250;
  static constexpr uint8_t FREE = 10;
  static constexpr uint8_t UPDATE_POS = 8;
  static constexpr uint8_t UPDATE_NEG = 2;

public:

  void init(uint8_t& cell) {
    cell = UNKNOWN;
  }

  void update_pos(uint8_t& cell) {
    int32_t new_value = static_cast<int32_t>(cell) + static_cast<int32_t>(UPDATE_POS);
    cell = static_cast<uint8_t>(std::min(static_cast<int32_t>(OCCUPIED), new_value));
  }

  void update_neg(uint8_t& cell) {
    int32_t new_value = static_cast<int32_t>(cell) - static_cast<int32_t>(UPDATE_NEG);
    cell = static_cast<uint8_t>(std::max(static_cast<int32_t>(FREE), new_value));
  }
};


template <class CellT,
          template <class, int> class GridT,
          template <class, int> class StorageT,
          class UpdaterT,
          int Dim>
class OccMap {
 public:
  typedef std::shared_ptr<OccMap> Ptr;
  typedef CellT CellType;
  typedef Eigen::Matrix<float, Dim, 1> Vecf;
  typedef Eigen::Matrix<float, Dim, Eigen::Dynamic> Matf;
  typedef Eigen::Matrix<grid_ix_t, Dim, 1> VecIx;

 public:
  OccMap() {

  }

  OccMap(const Vecf& center,
         const VecIx& dimension,
         float resolution) :
      grid_(center, dimension, resolution)
  {


    storage_.reset(dimension);

    CellT cell;
    updater_.init(cell);
    storage_.fill(cell);
  }

  void reset() {
    CellT cell;
    updater_.init(cell);
    storage_.fill(cell);
  }

  void update(const Vecf& originf,
              const Vecf& xyf) {
    bool hit = false, intersects = false;
    VecIx start_grid_ix(VecIx::Zero());
    VecIx end_grid_ix(VecIx::Zero());
    this->compute_start_end_grid_ix(originf,
                                    xyf,
                                    start_grid_ix,
                                    end_grid_ix,
                                    hit,
                                    intersects);
    if (!intersects) { return; }

    if (hit && grid_.is_inside_grid(end_grid_ix)) {

      mem_ix_t mem_ix = grid_.grid_to_mem(end_grid_ix);
      CellT& cell(storage_[mem_ix]);
      updater_.update_pos(cell);
    }

    BresenhamIterator<Dim> bitr(start_grid_ix, end_grid_ix);
    do {
      bitr.step();
      VecIx grid_ix(bitr.pos());
      // sometimes there's edge effects with clipping
      // so we ignore positions outside the grid

      // TODO consider other policies that map
      // grid_ix -> key
      // where key is not just memory. Or encapsulate
      // like map.update(grid_ix)

      // ideally would want
      // xyz0, xyz1 = endpoints
      // ijk0, ijk1 = fixedgrid.world_to_grid()
      // ijk = map.world
      // CellT& cell(store


      if (!bitr.done() && grid_.is_inside_grid(grid_ix)) {
        mem_ix_t mem_ix = grid_.grid_to_mem(grid_ix);
        CellT& cell(storage_[mem_ix]);
        updater_.update_neg(cell);
      }
    } while (!bitr.done());
  }

  void multi_update(const Matf& vp,
                    const Matf& p) {
    ROS_ASSERT(p.rows() == vp.rows());
    for (int i=0; i < p.cols(); ++i) {
      Vecf xyf(p.col(i));
      Vecf originf(vp.col(i));
      this->update(originf, xyf);
    }
  }

  void multi_update_single_vp(const Vecf& vp,
                              const Matf& p) {
    ROS_ASSERT(p.rows() == vp.rows());
    for (int i=0; i < p.cols(); ++i) {
      Vecf xyf(p.col(i));
      this->update(vp, xyf);
    }
  }


  void decay() {
    for (int i=0; i < grid_.num_cells(); ++i) {
      CellT& cell(storage_[i]);
      updater_.update_decay(cell);
    }
  }


  StorageT<CellT, Dim>& get_storage() {
    return storage_;
  }

  GridT<float, Dim>& get_grid() {
    return grid_;
  }

 private:
  void compute_start_end_grid_ix(const Vecf& origin,
                                 const Vecf& x,
                                 VecIx& start_grid_ix,
                                 VecIx& end_grid_ix,
                                 bool& hit,
                                 bool& intersects) {

    // inter1 and inter2 are clipped versions of origin, x

    auto box(grid_.box());
    Vecf inter1(Vecf::Zero());
    Vecf inter2(Vecf::Zero());
    intersects = box.clip_line(origin, x, inter1, inter2);

    if (!intersects) {
      start_grid_ix.setZero();
      end_grid_ix.setZero();
      hit = false;
    } else {
      start_grid_ix = grid_.world_to_grid(inter1);
      end_grid_ix = grid_.world_to_grid(inter2);
      hit = grid_.is_inside_box(x);
    }
  }


 private:
  GridT<float, Dim> grid_;
  StorageT<CellT, Dim> storage_;
  UpdaterT updater_;

};

typedef OccMap<float, FixedGrid, DenseArray, BinaryFloatUpdater, 2> FixedBinMap2f;
typedef OccMap<HitPassI4, FixedGrid, DenseArray, HitPassUpdater, 2> FixedHitPassMap2i;
typedef OccMap<float, FixedGrid, DenseArray, BinaryFloatUpdater, 3> FixedBinMap3f;
typedef OccMap<HitPassI4, FixedGrid, DenseArray, HitPassUpdater, 3> FixedHitPassMap3i;


}

#endif /* end of include guard: FIXEDOCCMAP_HPP_I9FPFYWV */
