/**
 * Copyright (c) 2015 Carnegie Mellon University, Daniel Maturana <dimatura@cmu.edu>
 *
 * For License information please see the LICENSE file in the root directory.
 *
 */

#ifndef DENSE_ARRAY_HPP_KYZBZH9N
#define DENSE_ARRAY_HPP_KYZBZH9N

#include <iostream>

#include <cstdint>
#include <stdexcept>
#include <type_traits>

#include <memory>
#include <vector>

#include <Eigen/Core>
#include <Eigen/Dense>

#include "voxgrid/grid_types.hpp"

namespace voxgrid {

/**
 * A dense ND array.
 * Maps ijk to an CellT.
 * No notion of origin, scrolling etc.
 * The *GridN classes handle that.
 * TODO: maybe use std::shared_array
 * TODO: Use Eigen::Ref<T> to avoid copying w/ pybind11
 *
 */
template<class CellT, int DimT>
class DenseArray {
public:
  static constexpr int Dim = DimT;
  using CellType = CellT;
  using ArrayType = CellT *;
  using iterator = CellT *;
  using const_iterator = const CellT *;
  using VecIx = VecDIx<Dim>;
  using VecMIx = VecDMIx<Dim>;
  using MatIx = MatDIx<Dim>;
  using Ptr = std::shared_ptr<DenseArray>;
  using ConstPtr = std::shared_ptr<const DenseArray>;


public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  // TODO perhaps stride/memory layout should
  // be further configurable.

  DenseArray() :
      dimension_(VecIx::Zero()),
      num_cells_(0),
      strides_(VecMIx::Zero()),
      grid_(nullptr),
      begin_(nullptr),
      end_(nullptr),
      owns_memory_(false)
  { }

  DenseArray(const VecIx& dimension) :
      dimension_(dimension),
      num_cells_(dimension.prod()),
      grid_(new CellT[num_cells_]()),
      begin_(&grid_[0]),
      end_(&grid_[0]+num_cells_),
      owns_memory_(true)
  {
    this->calc_strides();
  }

  /**
   * Used when wrapping an external chunk of data.
   */
  DenseArray(const VecIx& dimension, ArrayType grid_data) :
      dimension_(dimension),
      num_cells_(dimension.prod()),
      grid_(grid_data),
      begin_(&grid_[0]),
      end_(&grid_[0]+num_cells_),
      owns_memory_(false)
  {
    this->calc_strides();
  }

  DenseArray(const DenseArray& other) {
    dimension_ = other.dimension_;
    num_cells_ = other.num_cells_;
    strides_ = other.strides_;

    grid_ = new CellT[num_cells_]();
    begin_ = &(grid_[0]);
    end_ = &(grid_[0])+num_cells_;

    std::copy(other.begin(), other.end(), this->begin());
    owns_memory_ = true;
  }

  DenseArray& operator=(const DenseArray& other) = delete;

  void copy_from(const DenseArray& other) {
    this->reset(other.dimension());
    std::copy(other.begin(), other.end(), this->begin());
  }

  virtual ~DenseArray() {
    if (owns_memory_ && grid_) {
      delete[] grid_;
      grid_ = nullptr;
    }
  }

  void calc_strides() {
    // note tail(0) behaves like "1".
    // also note that units are indices, not bytes.
    // row major
    for (int i=0; i < Dim; ++i) {
      strides_(Dim-i-1) = dimension_.tail(i).prod();
    }
  }

  void reset(const VecIx& dimension) {
    dimension_ = dimension;
    num_cells_ = dimension.prod();
    this->calc_strides();
    if (owns_memory_ && grid_) { delete[] grid_; }
    // TODO we use new T[n]();
    // cost of ctor may or not be ok
    grid_ = new CellT[num_cells_]();
    begin_ = &(grid_[0]);
    end_ = &(grid_[0])+num_cells_;
  }

  void reset(const VecIx& dimension, ArrayType grid_data) {
    if (owns_memory_ && grid_) {
      delete[] grid_;
    }
    dimension_ = dimension;
    num_cells_ = dimension.prod();
    this->calc_strides();
    owns_memory_ = false;
    grid_ = grid_data;
    begin_ = &(grid_[0]);
    end_ = &(grid_[0])+num_cells_;
  }

  size_t allocated_bytes() const {
    return sizeof(CellT)*num_cells_;
  }

  void fill(const CellT& val) {
    std::fill(this->begin(), this->end(), val);
  }

public:
  iterator begin() const { return begin_; }
  iterator end() const { return end_; }

  const_iterator cbegin() const { return begin_; }
  const_iterator cend() const { return end_; }

public:

  template<typename T, typename... Targs>
  CellType& operator()(T ix0, Targs... ixrest) {
    // consider is_convertible
    static_assert(std::is_same<T, gix_t>::value,
                  "grid_to_mem must be called with gix_t");
    static_assert(sizeof...(ixrest) == (Dim-1), "bad number of args");
    return this->var_get_aux(0, 0, ix0, ixrest...);

  }

  VecIx mem_to_grid(gix_t mem_ix) const {
    VecIx out(VecIx::Zero());
    for (int i=0; i < Dim; ++i) {
      gix_t ix = mem_ix/strides_[i];
      mem_ix -= ix*strides_[i];
      out(i) = ix;
    }
    return out;
  }

  CellType& get(const VecIx& grid_ix) {
    gix_t mem_ix = this->grid_to_mem(grid_ix);
    return grid_[mem_ix];
  }

  const CellType& get(const VecIx& grid_ix) const {
    gix_t mem_ix = this->grid_to_mem(grid_ix);
    return grid_[mem_ix];
  }

  void set(const VecIx& grid_ix, const CellType& val) {
    gix_t mem_ix = this->grid_to_mem(grid_ix);
    grid_[mem_ix] = val;
  }

  mix_t grid_to_mem(const VecIx& grid_ix) const {
    return strides_.dot(grid_ix.template cast<mix_t>());
  }

  VecMIx multi_grid_to_mem(const MatIx& grid_indices) const {
    VecMIx out(grid_indices.rows());
    for (int ix=0; ix < grid_indices.rows(); ++ix) {
      VecIx gix(grid_indices.row(ix));
      out[ix] = this->grid_to_mem(gix);
    }
    return out;
  }

  VecIx mem_to_grid(mix_t mem_ix) const {
    VecIx out(VecIx::Zero());
    for (int i=0; i < Dim; ++i) {
      gix_t ix = mem_ix/strides_[i];
      mem_ix -= ix*strides_[i];
      out(i) = ix;
    }
    return out;
  }


  /**
   * Bound check
   */
  CellType& get_safe(gix_t mem_ix) {
    if (mem_ix < 0 || mem_ix >= num_cells_) {
      throw std::out_of_range("bad mem_ix");
    }
    return grid_[mem_ix];
  }

  /**
   * Bound check
   */
  const CellType& get_safe(gix_t mem_ix) const {
    if (mem_ix < 0 || mem_ix >= num_cells_) {
      throw std::out_of_range("bad mem_ix");
    }
    return grid_[mem_ix];
  }


  /**
   * No bound check
   */
  CellType& operator[](gix_t mem_ix) {
    return grid_[mem_ix];
  }

  /**
   * No bound check
   */
  const CellType& operator[](gix_t mem_ix) const {
    return grid_[mem_ix];
  }


public:
  // properties
  gix_t dim(int i) const { return dimension_[i]; }
  gix_t ndim() const { return Dim; }
  VecIx dimension() const { return dimension_; }
  gix_t num_cells() const { return num_cells_; }
  ArrayType data() const { return &grid_[0]; }
  gix_t stride(int i) const { return strides_[i]; }
  VecIx strides() const { return strides_; }


private:
  template<typename T, typename... Targs>
  CellType& var_get_aux(int i, mix_t mix, T ix0, Targs... ixrest) {
    static_assert(std::is_same<T, gix_t>::value,
                  "grid_to_mem must be called with gix_t");
    mix_t mix2 = (ix0*strides_[i]) + mix;
    return this->var_get_aux(i+1, mix2, ixrest...);
  }

  CellType& var_get_aux(int i, mix_t mix) {
    return grid_[mix];
  }


private:
  // number of grid cells along each axis
  VecIx dimension_;

  // number of cells
  gix_t num_cells_;

  // grid strides to translate from linear to 3D layout.
  // C-ordering, ie x slowest, z fastest.
  VecMIx strides_;

  ArrayType grid_;
  iterator begin_, end_;

  // does this object own the grid_ mem
  bool owns_memory_;

};

typedef DenseArray<float, 2> DenseArray2f;
typedef DenseArray<float, 3> DenseArray3f;

typedef DenseArray<std::int32_t, 2> DenseArray2i;
typedef DenseArray<std::int32_t, 3> DenseArray3i;

typedef DenseArray<vec2i_t, 2> DenseArray2Vec2i;
typedef DenseArray<vec2i_t, 3> DenseArray3Vec2i;

typedef DenseArray<vec2f_t, 2> DenseArray2Vec2f;
typedef DenseArray<vec2f_t, 3> DenseArray3Vec2f;

typedef DenseArray<vec3f_t, 2> DenseArray2Vec3f;
typedef DenseArray<vec3f_t, 3> DenseArray3Vec3f;
}

#endif /* end of include guard: DENSE_ARRAY_HPP_KYZBZH9N */
